package ClassResources;

import Main.Parser;
import javafx.beans.Observable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Observer;

public class MovieDatabase {

    private Map<String, Movie> movieMapByName = new HashMap<>();
    public static final MovieDatabase INSTANCE  =  new MovieDatabase();


    private MovieDatabase() {
    }




    public void addMovie(Parser parser) {
        String n = parser.getMovieName();
        String d = parser.getDirectorName();
        LocalDate rd = parser.getReleaseDate();
        MovieType typ = parser.getMovieType();
        movieMapByName.put(n, new Movie(n, typ, rd, d));
    }

    public void addMovie(Movie movie) {
        movieMapByName.put(movie.getMovieName(), movie);
    }

    // wyrzuca NullPointerE jak nie ma w bazie
    public Movie searchByName(String movieName) {
        return movieMapByName.get(movieName);
    }

    public void printAllMovies() {
        for (Movie movie : movieMapByName.values()) {
            System.out.println(movie);
            System.out.println();
        }

    }

    public void printAllMovies(MovieType type) {
        for (Movie movie : movieMapByName.values()) {
            if (movie.getMovieType().equals(type)) {
                System.out.println(movie);
                System.out.println();
            }
        }
    }

    public Map<String, Movie> getMovieMapByName() {
        return movieMapByName;
    }
}
