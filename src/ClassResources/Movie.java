package ClassResources;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Movie {
    private String movieName;
    private MovieType movieType;
    private LocalDate releaseDate;
    private String directorName;

    public Movie(String movieName, MovieType movieType, LocalDate releaseDate, String directorName) {
        this.movieName = movieName;
        this.movieType = movieType;
        this.releaseDate = releaseDate;
        this.directorName = directorName;
    }

    public String getMovieName() {
        return movieName;
    }


    public MovieType getMovieType() {
        return movieType;
    }



    public LocalDate getReleaseDate() {
        return releaseDate;
    }


    public String getDirectorName() {
        return directorName;
    }

    @Override
    public String toString() {
        return "MovieName: " + movieName + "\nMovieType: " + movieType + "\nYear: " + releaseDate + "\nDirectorName: " + directorName;
    }
}
