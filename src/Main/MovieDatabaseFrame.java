package Main;

import javax.swing.*;
import java.awt.*;

public class MovieDatabaseFrame extends JFrame{

    private MovieDatabaseLayout instance;

    public MovieDatabaseFrame() throws HeadlessException {
        this.instance = new MovieDatabaseLayout();
        setContentPane(instance.getPanel1());
        pack();
        //setMinimumSize(new Dimension(800, 600));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
