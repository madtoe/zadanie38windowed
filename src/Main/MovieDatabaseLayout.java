package Main;

import ClassResources.Movie;
import ClassResources.MovieDatabase;
import ClassResources.MovieType;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Scanner;

public class MovieDatabaseLayout {
    public JPanel getPanel1() {
        return panel1;
    }

    private JPanel panel1;
    private JTextField Nazwa;
    private JTable table1;
    private JButton Dodaj;
    private JButton Usun;
    private JComboBox Gatunek;
    private JComboBox Rok;
    private JButton Czytaj;
    private JButton Zapisz;
    private JTextField Rezyser;
    private JProgressBar pasek;
    private MovieTableModel model;
    private int zaznaczonyIndeks;
    private ProgressBarTask task;

    File file = new File("movieDatabase.txt");

    public MovieDatabaseLayout() {
        this.model = new MovieTableModel();
        table1.setModel(model);
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        Dodaj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("yyyy")
                        .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                        .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                        .toFormatter();
                LocalDate releaseDate = LocalDate.parse(Rok.getSelectedItem().toString(), dtf);


                Movie m = new Movie(Nazwa.getText(), MovieType.valueOf(String.valueOf(Gatunek.getSelectedItem()).toUpperCase()), releaseDate, Rezyser.getText());
                //MovieDatabase.INSTANCE.addMovie(m);
                model.addMovie(m);
            }
        });

        ListSelectionModel modelZaznaczenia = table1.getSelectionModel();
        modelZaznaczenia.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                zaznaczonyIndeks = table1.getSelectedRow();
                if (zaznaczonyIndeks == -1) {
                    Usun.setEnabled(false);
                } else Usun.setEnabled(true);
            }
        });


        Zapisz.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {
                    for (Movie movie : MovieDatabase.INSTANCE.getMovieMapByName().values()) {
                        pw.println(movie);
                        pw.println("-separator-");

                    }
                } catch (IOException oe) {
                    oe.printStackTrace();
                }
            }
        });
        Czytaj.addActionListener(new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent evt) {
                Czytaj.setEnabled(false);
                Czytaj.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                ProgressBarTask task = new ProgressBarTask();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if("progress" == evt.getPropertyName()){
                            int progress = (Integer) evt.getNewValue();
                            pasek.setValue(progress);
                        }
                    }
                });
                task.execute();
            }

        });
        Usun.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeMovie(model.map.get(model.map.keySet().toArray()[zaznaczonyIndeks]));
            }
        });

    }
    class ProgressBarTask extends javax.swing.SwingWorker<Void, Void> {
        boolean done;

        public void done() {
            done = true;
//            Toolkit.getDefaultToolkit().beep();
            Czytaj.setEnabled(true);
            Czytaj.setCursor(null);
            pasek.setValue(pasek.getMinimum());
        }

        @Override
        protected Void doInBackground() {
            model.clear();
            model.fireTableDataChanged();
            setProgress(0);
            for (int i = 0; i < 101; i++) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgress(i);
            }
            try {
                Scanner fileScan = new Scanner(file);
                Parser readParser = new Parser();
                while (fileScan.hasNextLine()) {
                    String line = fileScan.nextLine();
                    String[] lineArguments = line.split(":");
                    String keyword = lineArguments[0];

                    if (keyword.contains("MovieName")) {
                        readParser.setMovieName(lineArguments[1].trim());

                    } else if (keyword.contains("MovieType")) {
                        readParser.setMovieType(MovieType.valueOf(lineArguments[1].trim()));
                    } else if (keyword.contains("Year")) {
                        readParser.setReleaseDate(LocalDate.parse(lineArguments[1].trim()));
                    } else if (keyword.contains("DirectorName")) {
                        readParser.setDirectorName(lineArguments[1].trim());
                    } else if (keyword.contains("separator")) {
                        MovieDatabase.INSTANCE.addMovie(readParser);
                    }
                }
            } catch (FileNotFoundException fe) {
                fe.printStackTrace();
            }
            model.fireTableDataChanged();
            return null;
        }
    }

}
