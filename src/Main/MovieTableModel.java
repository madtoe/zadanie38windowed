package Main;

import ClassResources.Movie;
import ClassResources.MovieDatabase;

import javax.swing.table.AbstractTableModel;
import java.util.Map;

public class MovieTableModel extends AbstractTableModel{

    Map<String, Movie> map = MovieDatabase.INSTANCE.getMovieMapByName();
    private final String[] columnNames = new String[] {"Nazwa", "Gatunek", "Data premiery", "Reżyser"};

    public void addMovie(Movie movie){
        map.put(movie.getMovieName(), movie);
        fireTableDataChanged();
    }

    public void removeMovie(Movie movie){
        map.remove(movie.getMovieName());
        fireTableDataChanged();
    }

    public MovieTableModel() {
    }

    public void clear(){
        map.clear();
    }

    @Override
    public String getColumnName(int column) {
        return this.columnNames[column];
    }

    @Override
    public int getRowCount() {
        return map.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Movie tmp = map.get(map.keySet().toArray()[rowIndex]);
        switch (columnIndex) {
            case 0: // co ma wyświetlić kolumna 0
                return tmp.getMovieName();
            case 1: // co ma wyświetlić kolumna 1
                return tmp.getMovieType();
            case 2: // co ma wyświetlić kolumna 2
                return tmp.getReleaseDate();
            case 3: // co ma wyświetlić kolumna 3
                return tmp.getDirectorName();
            default: // co ma wyświetlić każda inna kolumna
                return "unknown";
        }
    }


}
